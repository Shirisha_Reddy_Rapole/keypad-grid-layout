package com.example.s530484.rapole_assignment03;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void testFunction(View v)
    {
        Button button = findViewById(v.getId());
        TextView number = findViewById(R.id.number);
        String str = button.getText().toString();
        int num = Integer.parseInt(str);
        number.append(num + "");

    }

    public void openFunction(View v)
    {
        View bgcolor = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        ImageView img = (ImageView) findViewById(R.id.lockImage);
        TextView number = findViewById(R.id.number);
        TextView lock = findViewById(R.id.lock);
        try {
            String n = number.getText().toString();
            int num = new Integer(n);
            int compare = 98441;
            if (num == compare) {
                lock.setText("UNLOCKED");
                img.setImageResource(R.drawable.unlock);
                bgcolor.setBackgroundResource(R.color.colorPrimary);
            } else {
                lock.setText("LOCKED");
                img.setImageResource(R.drawable.locked);
                bgcolor.setBackgroundResource(R.color.colorAccent);
            }
        }
        catch(NumberFormatException e)
        {
            lock.setText("Enter a valid number");
        }
    }

    public void deleteFunction(View v)
    {
        TextView number = findViewById(R.id.number);
        String n = number.getText().toString();
        String str="";
        for(int i=0; i<n.length()-1;i++)
        {
            str+=n.charAt(i)+"";
        }
        number.setText(str);
    }

    public void resetFunction(View v)
    {
        View bgcolor = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        TextView number = findViewById(R.id.number);
        TextView lock = findViewById(R.id.lock);
        ImageView img=(ImageView)findViewById(R.id.lockImage);
        number.setText("");
        lock.setText("LOCKED");
        img.setImageResource(R.drawable.pic1);
        bgcolor.setBackgroundResource(R.drawable.pic1);
    }
}
