##Goal: Build a simple app that will simulate a keypad entry system.

###Requirements:
-You will have two TextViews at the top of the screen.  
a. The code text view will be used to display the digits entered on the
keypad.  
b. The status text view will display "LOCKED" or "UNLOCKED"
depending on the status of the entry.  
-You will need a 3 by 3 grid layout with nine buttons labeled 1 to 9. Whenever
a button is pressed, add the digit t the code text view.  
-You will need two operation buttons  
a. The OPEN button will check the entered code against "98441". If they
match, then set the statues to UNLOCKED. Otherwise, set it to
LOCKED.  
b. The RESET button will clear the code text view and set the status to
LOCKED.  
-Make sure the label of the app in the manifest is "KeyPad Entry"  
###Bonus:  
! Use color to improve the look of the app. (Restraint is the key here.)  
! Use images on the buttons to improve the look and feel of the app.  
! Change the background color of the app depending on if the keypad is locked
or unlocked.  
! Add an ImageView that shows a closed or open lock image depending on the
status.  
! Add an edit text that can be used to change the code to match for unlocking.
You will want a variable to store the value that you declare in the activity
class.  
! Add a zero to the keypad.  
! Change the action of the buttons so that only the last digit is displayed and
the rest of the digits are *.  